//
//  MasterInteractor.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation
import UIKit
class MasterInteractor: MasterInteractorProtocol {
    weak var presenter: MasterPresenterProtocol!
    var rssXmlParser: RssXmlParserProtocol!
    var channelsShown: Array<Channel> = []
    
    var channel: Channel?
    
    var channelURLs: Array<ChannelPreset> = []
    private var urls = ["http://images.apple.com/main/rss/hotnews/hotnews.rss",
                "https://www.theguardian.com/international/rss", "https://lenta.ru/rss", "https://tvrain.ru/export/rss/all.xml", "https://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml" ]
    
    init(presenter: MasterPresenterProtocol) {
        self.presenter = presenter
        self.rssXmlParser = RssXmlParser()
        
        channelURLs.append(ChannelPreset(withTitle: "Apple Hot News", andURL: urls[0]))
        channelURLs.append(ChannelPreset(withTitle: "The Guardian International", andURL: urls[1]))
        channelURLs.append(ChannelPreset(withTitle: "Lenta.ru", andURL: urls[2]))
        channelURLs.append(ChannelPreset(withTitle: "TV Rain", andURL: urls[3]))
        channelURLs.append(ChannelPreset(withTitle: "New York Times", andURL: urls[4]))
    }
    
    func retrieveData(url: URL) {
        channel = self.rssXmlParser.loadXml(url: url)
        if channel != nil {
            self.loadImageForItems()
        }
        presenter.loaded(channel: channel)
    }
    
    func loadImageForItems() {
        for itemIndex in 0..<channel!.items.count {
            var smallestSize: Int = 1000
            var largestSize: Int = 0
            var linkToSmallestSize: String = ""
            var linkToLargestSize: String = ""
            for media in channel!.items[itemIndex].mediaContent {
                if media.width != 0 &&
                    media.width < smallestSize {
                    smallestSize = media.width
                    linkToSmallestSize = media.url
                }
                if media.width != 0 &&
                    media.width > largestSize {
                    largestSize = media.width
                    linkToLargestSize = media.url
                }
            }
            if linkToSmallestSize == "" && linkToLargestSize == "" && channel!.items[itemIndex].mediaContent.count > 0 {
                linkToSmallestSize = channel!.items[itemIndex].mediaContent[0].url
                linkToLargestSize = channel!.items[itemIndex].mediaContent[0].url
            }
            if let url = URL(string: linkToSmallestSize) {
                channel!.items[itemIndex].imagePreview = self.load(url: url)
            }
            channel!.items[itemIndex].imageFullSizeLink = linkToLargestSize
        }
    }
    
    func load(url: URL) -> UIImage? {
        if let data = try? Data(contentsOf: url) {
            return UIImage(data: data)
        } else {
            return nil
        }
    }
}
