//
//  MasterAssembly.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation

class MasterAssembly: MasterAssemblyProtocol {
    func assemble(with viewController: MasterViewController) {
        let presenter = MasterPresenter(view: viewController)
        let interactor = MasterInteractor(presenter: presenter)
        let router = MasterRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
