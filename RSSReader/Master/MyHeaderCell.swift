//
//  MyHeaderCell.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 28/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation
import UIKit

class MyHeaderCell: UITableViewCell {
    
    var starFilled: Bool = false
    
    @IBOutlet weak var starButton: UIButton!
    
    var selectFeedAsFavourite: ((Bool) -> Void)?
    var buttonAction: ((UIButton) -> Void)?
    
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBAction func starButtonTouchUpInside(_ sender: UIButton) {
        self.updateStar(favourite: !starFilled)
        self.selectFeedAsFavourite?(starFilled)
    }
    
    @IBAction func deleteButtonPressed(_ sender: UIButton) {
        self.buttonAction?(sender)
    }

    func updateStar(favourite: Bool) {
        if favourite {
            starButton.setImage(UIImage(named: "star_filled"), for: UIControl.State.normal)
        } else {
            starButton.setImage(UIImage(named: "star"), for: UIControl.State.normal)
        }
        starFilled = favourite
    }
}
