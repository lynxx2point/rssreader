//
//  MasterRouter.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation

class MasterRouter: MasterRouterProtocol {
    weak var viewController: MasterViewController!
    
    init(viewController: MasterViewController) {
        self.viewController = viewController
    }
}
