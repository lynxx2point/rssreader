//
//  MasterPresenter.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation

class MasterPresenter: MasterPresenterProtocol {
    weak var view: MasterViewProtocol!
    var interactor: MasterInteractorProtocol!
    var router: MasterRouterProtocol!

    var urlForDownload: URL?
    var isDownloadingCustomUrl: Bool!
    var urlToAdd: ChannelPreset?
    var customUrlToAdd: String?
    var channelUrlToAdd: String?
    var alreadyLoadedChannelPresets: Array<ChannelPreset> = []
    var showOnlyFavourite: Bool = false
    var loadedChannelUrls: Array<URL> = []
    var channels: Array<Channel> = []
    
    var favouriteChannels: Array<Channel> {
        var favouriteChannels: Array<Channel> = []
        for channel in channels {
            if channel.isFavourite {
                favouriteChannels.append(channel)
            }
        }
        return favouriteChannels
    }
    
    var notLoadedPresets: [ChannelPreset] {
        var presets: Array<ChannelPreset> = []
        for channelInfo in interactor.channelURLs {
            var shouldNotAdd: Bool = false
            for url in self.loadedChannelUrls {
                if channelInfo.url == url {
                    shouldNotAdd = true
                }
            }
            if !shouldNotAdd {
                presets.append(channelInfo)
            }
        }
        return presets
    }
    
    var urlPresetsData: [String] {
        var titles: Array<String> = []
        for preset in notLoadedPresets {
            titles.append(preset.title)
        }
        return titles
    }
    
    init(view: MasterViewProtocol) {
        self.view = view
    }
    
    func configureView() {
        self.view.initUI()
    }
    
    func showError(with text: String) {
        view.showAlertView(with: text)
    }
    
    func loadFromCustom() {
        let urlString = self.customUrlToAdd
        if urlString != nil && urlString != "" {
            if let url = URL(string: urlString!) {
                self.isDownloadingCustomUrl = true
                self.loadChannel(url: url)
            } else {
                view.showMessage(text: "Invalid URL. Could not load feed")
            }
        }
    }
    
    func loadFromPreset() {
        if let urlValid = self.urlToAdd?.url {
            self.isDownloadingCustomUrl = false
            self.loadChannel(url: urlValid)
        } else {
            view.showMessage(text: "Can't load from presets")
        }
    }
    
    func presetSelected(index: Int?) {
        if let indexExists = index {
            if notLoadedPresets.count > indexExists {
                self.urlToAdd = notLoadedPresets[indexExists]
            }
        } else {
            self.urlToAdd = nil
        }
    }
    
    func loadChannel(url: URL) {
        if !self.loadedChannelUrls.contains(url) {
            view.loadingState(enable: true)
            self.urlForDownload = url
            view.showMessage(text: "Please wait while the feed is being loaded...")
            DispatchQueue.global(qos: .default).async {
                self.interactor.retrieveData(url: url)
            }
        } else {
            view.showMessage(text: "This URL has already been added")
        }
    }
    
    func loaded(channel: Channel?) {
        DispatchQueue.main.async {
            if let channelLoaded = channel {
                self.loadedChannelUrls.append(self.urlForDownload!)
                if !self.isDownloadingCustomUrl {
                    self.presetSelected(index: self.view.reloadPickerAndReturnRow())
                } else {
                    self.view.clearTextField()
                }
                self.addSection(data: channelLoaded)
                self.view.showMessage(text: "Success! Feed has been loaded")
            } else {
                self.view.showMessage(text: "Sorry. Could not load feed")
            }
            self.isDownloadingCustomUrl = nil
            self.view.loadingState(enable: false)
        }
    }
    
    func addChannelBtnPressed() {
        // default selection is first row of preset picker view
        presetSelected(index: 0)
        view.showAddChannelView()
    }

    func addSection(data: Channel) {
        self.channels.append(data)
        view.updateTable()
    }
    
    func removeSection(index: Int) {
        if showOnlyFavourite {
            let channelToRemove = favouriteChannels[index]
            var indexToRemove: Int?
            for i in 0..<channels.count {
                if channels[i] === channelToRemove {
                    indexToRemove = i
                }
                if let indexToRemoveFound = indexToRemove {
                    self.removeChannel(index: indexToRemoveFound)
                    break
                }
            }
            self.view.updateTable()
        } else {
            self.removeChannel(index: index)
            self.view.updateTable()
        }
    }
    
    func removeChannel(index: Int) {
        channels.remove(at: index)
        self.loadedChannelUrls.remove(at: index)
    }
    
    func channelItemsToPass(sectionSelected: Int) -> [ChannelItem] {
        if self.showOnlyFavourite {
           return self.favouriteChannels[sectionSelected].items
        } else {
           return channels[sectionSelected].items
        }
    }
    
}
