//
//  MasterProtocols.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation

protocol MasterViewProtocol: class {
    func initUI()
    func showAlertView(with text: String)
    func updateTable()
    func showMessage(text: String)
    func loadingState(enable: Bool)
    func showAddChannelView()
    func reloadPickerAndReturnRow() -> Int?
    func clearTextField()
}

protocol MasterRouterProtocol: class {
    
}

protocol MasterAssemblyProtocol: class {
    func assemble(with viewController: MasterViewController)
}

protocol MasterPresenterProtocol: class {
    var view: MasterViewProtocol! { get set }
    var interactor: MasterInteractorProtocol! { get set }
    var router: MasterRouterProtocol! { get set }
    
    var customUrlToAdd: String? { get set }
    var showOnlyFavourite: Bool { get set }
    var favouriteChannels: Array<Channel> { get }
    var channels: Array<Channel> { get set }
    var urlPresetsData: [String] { get }
    
    func configureView()
    func showError(with text: String)
    func loaded(channel: Channel?)
    func presetSelected(index: Int?)
    func loadFromCustom()
    func loadFromPreset()
    func addChannelBtnPressed()
    func channelItemsToPass(sectionSelected: Int) -> [ChannelItem]
    func removeSection(index: Int)
}

protocol MasterInteractorProtocol: class {
    func retrieveData(url: URL)
    var channelURLs: [ChannelPreset] { get }
    var channelsShown: Array<Channel> { get set }
}
