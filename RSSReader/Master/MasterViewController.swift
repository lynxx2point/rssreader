//
//  MasterViewController.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import UIKit
import Popover
import SwiftOverlays

class MasterViewController: UITableViewController, MasterViewProtocol, LoadChannelViewDelegate {
    var detailViewController: DetailViewController? = nil
    var presenter: MasterPresenterProtocol!
    let assembly: MasterAssemblyProtocol = MasterAssembly()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assembly.assemble(with: self)
        presenter.configureView()
    }

    func initUI() {
        let filterButton = UIButton.init(type: UIButton.ButtonType.system)
        filterButton.setTitle("Filter", for: UIControl.State.normal)
        filterButton.addTarget(self, action: #selector(filterFeedsBtnPressed(_:)), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: filterButton)
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addChannelBtnPressed(_:)))
        navigationItem.rightBarButtonItem = addButton
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    @objc func filterFeedsBtnPressed(_ sender: Any) {
        if self.popover == nil {
            self.popover = Popover(options: nil, showHandler: nil, dismissHandler: nil)
            popoverTopOffset = popover.arrowSize.height
            self.createPopover()
        } else {
            let popoverHeight: CGFloat = 100 - popoverTopOffset
            let popoverWidth: CGFloat = 240.0
            self.popoverView.frame = CGRect(x: 0, y: 0, width: popoverWidth, height: popoverHeight)
        }
        popover.show(self.popoverView, fromView: navigationItem.leftBarButtonItem!.customView!)
    }
    
    @objc
    func addChannelBtnPressed(_ sender: Any) {
        presenter.addChannelBtnPressed()
    }
    
    var customViewMessageLabel: UILabel!
    var customViewController: LoadChannelViewController!
    
    func showAddChannelView() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        customViewController = mainStoryboard.instantiateViewController(withIdentifier: "load_channel_vc") as? LoadChannelViewController
        customViewController.delegate = self
        
        customViewMessageLabel = (customViewController.view.viewWithTag(2) as! UILabel)
        customViewMessageLabel.text = "Enter URL for the RSS feed you wish to add or choose one of the presets from the list"
        
        let btnUrl = (customViewController.view.viewWithTag(7) as! UIButton)
        btnUrl.addTarget(self, action: #selector(loadFromUrlBtnPressed(_:)), for: .touchUpInside)
        let btnPresets = (customViewController.view.viewWithTag(8) as! UIButton)
        btnPresets.addTarget(self, action: #selector(loadFromPresetBtnPressed(_:)), for: .touchUpInside)

        show(customViewController, sender: self)
    }
    
    @objc
    func loadFromUrlBtnPressed(_ sender: Any) {
        self.presenter.loadFromCustom()
    }
    
    @objc
    func loadFromPresetBtnPressed(_ sender: Any) {
        self.presenter.loadFromPreset()
    }
    
    func loadingState(enable: Bool) {
        if enable {
            UIApplication.shared.beginIgnoringInteractionEvents()
            self.customViewController.showWaitOverlay()
        } else {
            self.customViewController.removeAllOverlays()
            if UIApplication.shared.isIgnoringInteractionEvents {
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
    }
    
    func reloadPickerAndReturnRow() -> Int? {
        return customViewController.reloadPickerAndReturnRow()
    }
    
    func clearTextField() {
        self.customViewController.clearTextField()
    }
    
    func showMessage(text: String) {
        self.customViewMessageLabel.text = text
    }

    func updateTable() {
        self._tableView.reloadData()
    }
    
    // MARK: - LoadChannelViewDelegate
    
    func chosePresetUrlFromPicker(index: Int?) {
        presenter.presetSelected(index: index)
    }
    
    func enteredCustomUrlInTextField(urlString: String) {
        presenter.customUrlToAdd = urlString
    }
    
    func urlPresetsData() -> [String] {
        return presenter.urlPresetsData
    }
    
    // MARK: - Alerts
    
    func showAlertView(with text: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "", message: text, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                
                controller.sectionData = presenter.channelItemsToPass(sectionSelected: indexPath.section)
                controller.itemIndex = indexPath.row
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
    
    // MARK: - Popover
    
    var favouriteCheckmark: UIImageView!
    var allCheckmark: UIImageView!
    var popover: Popover!
    var popoverView: UIView!
    var popoverTopOffset: CGFloat = 0.0
    
    func createPopover() {
        let popoverHeight: CGFloat = 100 - popoverTopOffset
        let popoverWidth: CGFloat = 240.0
        self.popoverView = UIStackView(frame: CGRect(x: 0, y: 0, width: popoverWidth, height: popoverHeight))
        let rowView1 = UIView(frame: CGRect(x: 0, y: popoverTopOffset, width: popoverWidth, height: popoverHeight / 2))
        let rowView2 = UIView(frame: CGRect(x: 0, y: popoverTopOffset + popoverHeight / 2, width: popoverWidth, height: popoverHeight / 2))
        popoverView.addSubview(rowView1)
        popoverView.addSubview(rowView2)
        let buttonShowAll = UIButton(type: UIButton.ButtonType.custom)
        buttonShowAll.setTitle("Show all feeds", for: .normal)
        buttonShowAll.setTitleColor(UIColor.black, for: .normal)
        buttonShowAll.setTitleColor(UIColor.gray, for: .highlighted)
        buttonShowAll.translatesAutoresizingMaskIntoConstraints = false
        buttonShowAll.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        let buttonShowFavourites = UIButton(type: UIButton.ButtonType.custom)
        buttonShowFavourites.setTitle("Show favourite feeds", for: .normal)
        buttonShowFavourites.setTitleColor(UIColor.black, for: .normal)
        buttonShowFavourites.setTitleColor(UIColor.gray, for: .highlighted)
        buttonShowFavourites.translatesAutoresizingMaskIntoConstraints = false
        buttonShowFavourites.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
        rowView1.addSubview(buttonShowFavourites)
        rowView2.addSubview(buttonShowAll)
        
        buttonShowAll.addTarget(self, action: #selector(showAll(_:)), for: .touchUpInside)
        buttonShowFavourites.addTarget(self, action: #selector(showOnlyFavourite(_:)), for: .touchUpInside)
        
        let checkmarkImg = UIImage(named: "checkmark")
        self.favouriteCheckmark = UIImageView(image: checkmarkImg)
        self.favouriteCheckmark.translatesAutoresizingMaskIntoConstraints = false
        self.favouriteCheckmark.contentMode = UIView.ContentMode.scaleAspectFit
        self.favouriteCheckmark.isHidden = !presenter.showOnlyFavourite
        
        self.allCheckmark = UIImageView(image: checkmarkImg)
        self.allCheckmark.contentMode = UIView.ContentMode.scaleAspectFit
        self.allCheckmark.translatesAutoresizingMaskIntoConstraints = false
        self.allCheckmark.isHidden = presenter.showOnlyFavourite
        rowView1.addSubview(self.favouriteCheckmark)
        rowView2.addSubview(self.allCheckmark)
        NSLayoutConstraint.activate([
            buttonShowAll.leadingAnchor.constraint(equalTo: rowView2.leadingAnchor, constant: 10.0),
            buttonShowAll.centerYAnchor.constraint(equalTo: rowView2.centerYAnchor, constant: 0.0),
            buttonShowAll.heightAnchor.constraint(equalToConstant: 20.0),
            buttonShowAll.trailingAnchor.constraint(equalTo: self.allCheckmark.leadingAnchor, constant: 10.0),
            buttonShowFavourites.leadingAnchor.constraint(equalTo: rowView1.leadingAnchor, constant: 10.0),
            buttonShowFavourites.centerYAnchor.constraint(equalTo: rowView1.centerYAnchor, constant: 0.0),
            buttonShowFavourites.heightAnchor.constraint(equalToConstant: 20.0),
            buttonShowFavourites.trailingAnchor.constraint(equalTo: self.favouriteCheckmark.leadingAnchor, constant: 10.0),
            self.favouriteCheckmark.trailingAnchor.constraint(equalTo: rowView1.trailingAnchor, constant: -10.0),
            self.favouriteCheckmark.centerYAnchor.constraint(equalTo: rowView1.centerYAnchor, constant: 0.0),
            self.favouriteCheckmark.heightAnchor.constraint(equalToConstant: 20.0),
            self.favouriteCheckmark.widthAnchor.constraint(equalToConstant: 20.0),
            self.allCheckmark.trailingAnchor.constraint(equalTo: rowView2.trailingAnchor, constant: -10.0),
            self.allCheckmark.centerYAnchor.constraint(equalTo: rowView2.centerYAnchor, constant: 0.0),
            self.allCheckmark.heightAnchor.constraint(equalToConstant: 20.0),
            self.allCheckmark.widthAnchor.constraint(equalToConstant: 20.0),
            ])
    }
    
    @objc func showOnlyFavourite(_ sender: Any) {
        presenter.showOnlyFavourite = true
        self.allCheckmark.isHidden = true
        self.favouriteCheckmark.isHidden = false
        self._tableView.reloadData()
        self.popover.dismiss()
    }
    
    @objc func showAll(_ sender: Any) {
        presenter.showOnlyFavourite = false
        self.allCheckmark.isHidden = false
        self.favouriteCheckmark.isHidden = true
        self._tableView.reloadData()
        self.popover.dismiss()
    }

    // MARK: - Table View

    @IBOutlet var _tableView: UITableView!
    let cellId = "Cell"
    let noImageCellId = "NoImageCell"
    let headerCellId = "HeaderCell"
    let rowHeight: CGFloat = CGFloat(100.0)
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if presenter.showOnlyFavourite {
            return presenter.favouriteChannels.count
        } else {
            return presenter.channels.count
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if presenter.showOnlyFavourite {
            return presenter.favouriteChannels[section].items.count
        } else {
            return presenter.channels[section].items.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var object: ChannelItem!
        if presenter.showOnlyFavourite {
            object = presenter.favouriteChannels[indexPath.section].items[indexPath.row]
        } else {
            object = presenter.channels[indexPath.section].items[indexPath.row]
        }
        
        let identifier = object.imagePreview == nil ? self.noImageCellId : self.cellId
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)

        (cell.contentView.viewWithTag(1) as! UILabel).text = object.title
        
        let htmlData = NSString(string: object.description).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType:
            NSAttributedString.DocumentType.html]
        let attributedDescription = try? NSMutableAttributedString(data: htmlData ?? Data(),
                                                              options: options,
                                                              documentAttributes: nil)
        (cell.contentView.viewWithTag(2) as! UILabel).attributedText = attributedDescription
        
        if identifier == self.cellId {
            (cell.contentView.viewWithTag(3) as! UIImageView).image = object.imagePreview
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.headerCellId) as! MyHeaderCell
        var currentChannel: Channel!
        if presenter.showOnlyFavourite {
            currentChannel = presenter.favouriteChannels[section]
        } else {
            currentChannel = presenter.channels[section]
        }
        (cell.contentView.viewWithTag(1) as! UILabel).text = currentChannel.title
        cell.buttonAction = { sender in
            self.presenter.removeSection(index: section)
        }
        cell.selectFeedAsFavourite = { isFavourite in
            currentChannel.isFavourite = isFavourite
            
            if self.presenter.showOnlyFavourite && !isFavourite {
                // hide this feed
                self._tableView.reloadData()
            }
        }
        cell.updateStar(favourite: currentChannel.isFavourite)
        return cell
    }

    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath)  -> CGFloat {
        return self.rowHeight
    }


}

