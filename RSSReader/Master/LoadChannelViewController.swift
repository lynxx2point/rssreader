//
//  PopupDialogViewController.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 27/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation
import UIKit

protocol LoadChannelViewDelegate {
    func chosePresetUrlFromPicker(index: Int?)
    func enteredCustomUrlInTextField(urlString: String)
    func urlPresetsData() -> [String]
}

class LoadChannelViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var _textField: UITextField!
    @IBOutlet weak var _pickerView: UIPickerView!
    
    var delegate: LoadChannelViewDelegate!
    var pickerData: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self._pickerView.dataSource = self
        self._pickerView.delegate = self
        self._textField.delegate = self
        self._textField.keyboardType = UIKeyboardType.asciiCapable
        
        pickerData = self.delegate.urlPresetsData()
        if pickerData.count == 0 {
            self._pickerView.isHidden = true
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView,
                    didSelectRow row: Int,
                    inComponent component: Int) {
        self.delegate.chosePresetUrlFromPicker(index: row)
    }

    @IBAction func editingChanged(_ sender: Any) {
        self.delegate.enteredCustomUrlInTextField(urlString: (sender as! UITextField).text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func clearTextField() {
        self._textField.text = ""
    }
    
    func reloadPickerAndReturnRow() -> Int? {
        pickerData = self.delegate.urlPresetsData()
        if pickerData.count != 0 {
            self._pickerView.reloadAllComponents()
            return self._pickerView.selectedRow(inComponent: 0)
        } else {
            self._pickerView.isHidden = true
            return nil
        }
    }

}
