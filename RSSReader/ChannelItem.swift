//
//  ChannelItem.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 26/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation
import UIKit

struct ChannelItem {
    var title: String = ""
    var url: String = ""
    var description: String = ""
    var categories: Array<Category> = []
    var pubDate: String = ""
    var mediaContent: Array<Media> = []
    var imagePreview: UIImage!
    var imageFullSizeLink: String = ""
}

struct Media {
    var width: Int = 0
    var url: String = ""
    var credit: String = ""
}

struct Category {
    var domain: String = ""
    var name: String = ""
}
