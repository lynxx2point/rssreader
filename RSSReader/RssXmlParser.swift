//
//  XmlParser.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 26/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation
import UIKit

protocol RssXmlParserProtocol: class {
    func loadXml(url: URL) -> Channel?
}

class RssXmlParser: NSObject, RssXmlParserProtocol, XMLParserDelegate {
    var parser: XMLParser!
    var channel: Channel!
    var item: ChannelItem!
    var media: Media!
    var image: Image!
    var category: Category!
    var currentElement: String!
    var currentItem: CurrentItem = .channel
    
    func loadXml(url: URL) -> Channel? {
        channel = nil
        parser = XMLParser.init(contentsOf: url)
        parser.delegate = self
        parser.shouldResolveExternalEntities = false
        parser.parse() 
        parser = nil
        return channel
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        self.currentElement = elementName;
        switch elementName {
        case "channel":
            channel = Channel()
            currentItem = .channel
        case "item":
            item = ChannelItem()
            currentItem = .item
        case "image":
            image = Image()
            currentItem = .image
        case "media:content":
            media = Media()
            if let width = attributeDict["width"] {
                media!.width = (Int(width)) ?? 0
            }
            if let mediaUrl = attributeDict["url"] {
                media!.url = mediaUrl
            }
        case "enclosure":
            media = Media()
            if let mediaUrl = attributeDict["url"] {
                media!.url = mediaUrl
            }
        case "category":
            category = Category()
            if let domain = attributeDict["domain"] {
                category.domain = domain
            }
        default:
            return
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        switch self.currentElement {
        case "title":
            switch self.currentItem {
            case .channel:
                channel!.title += string
            case .item:
                item!.title += string
            case .image:
                image!.title += string
            }
        case "link":
            switch self.currentItem {
            case .channel:
                channel!.url += string
            case .item:
                item!.url += string
            default:
                return
            }
        case "description":
            switch self.currentItem {
            case .channel:
                channel!.description += string
            case .item:
                item!.description += string
            default:
                return
            }
        case "language":
            channel.language += string
        case "copyright":
            channel.copyright += string
        case "pubDate":
            switch self.currentItem {
            case .channel:
                channel!.pubDate += string
            case .item:
                item!.pubDate += string
            default:
                return
            }
        case "url":
            switch self.currentItem {
            case .image:
                image!.url += string
            default:
                return
            }
        case "category":
            category!.name += string
        case "media:credit":
            media!.credit += string
        default:
            return
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        switch elementName {
        case "channel":
            return
        case "item":
            channel!.items.append(item)
        case "image":
            channel!.image = image
        case "media:content":
            item!.mediaContent.append(media)
        case "enclosure":
            item!.mediaContent.append(media)
        default:
            return
        }
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        
    }
    
    enum CurrentItem {
        case channel
        case item
        case image
    }
}
