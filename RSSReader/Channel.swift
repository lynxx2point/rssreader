//
//  Feed.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 26/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation

class Channel {
    var title: String = ""
    var url: String = ""
    var description: String = ""
    var language: String = ""
    var copyright: String = ""
    var pubDate: String = ""
    var image: Image!
    var items: Array<ChannelItem> = []
    var isFavourite: Bool = false
}

struct Image {
    var title: String = ""
    var url: String = ""
}

struct ChannelPreset {
    var title: String = ""
    var url: URL!
    
    init(withTitle:String, andURL:String) {
        self.title = withTitle
        self.url = URL(string: andURL)
    }
}
