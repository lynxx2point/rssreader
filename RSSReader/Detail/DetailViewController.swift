//
//  DetailViewController.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import UIKit
import SwiftOverlays

class DetailViewController: UIViewController, DetailViewProtocol {
    
    var presenter: DetailPresenterProtocol!
    let assembly: DetailAssemblyProtocol = DetailAssembly()
    
    @IBOutlet weak var imageViewHeightConstraint: NSLayoutConstraint!
    
    var sectionData: [ChannelItem]!
    var itemIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        assembly.assemble(with: self)
        presenter.configureView(channelData: self.sectionData, itemIndex: self.itemIndex)
    }
    
    func initView(withImage: UIImage?, fromLeft: Bool?, forItem: ChannelItem) {
        let titleLabel = self.view.viewWithTag(1) as! UILabel
        let textView = self.view.viewWithTag(2) as! UITextView
        let imageView = self.view.viewWithTag(3) as! UIImageView
        
        var animation: CATransition!
        if fromLeft != nil {
            animation = CATransition.init()
            animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            animation.type = CATransitionType.push
            animation.duration = 0.25
            animation.subtype = fromLeft! ? CATransitionSubtype.fromLeft : CATransitionSubtype.fromRight
            
            titleLabel.layer.add(animation, forKey:"kCATransitionFade")
            textView.layer.add(animation, forKey:"kCATransitionFade")
            imageView.layer.add(animation, forKey:"kCATransitionFade")
        }
        
        titleLabel.text = forItem.title
        
        let htmlData = NSString(string: forItem.description).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType:
            NSAttributedString.DocumentType.html]
        let attributedDescription = try? NSMutableAttributedString(data: htmlData ?? Data(),
                                                                   options: options,
                                                                   documentAttributes: nil)
        
        textView.attributedText = attributedDescription
        
        if withImage != nil {
            imageViewHeightConstraint.constant = withImage!.size.height
            imageView.image = withImage
        } else {
            imageViewHeightConstraint.constant = 0
            imageView.image = nil
        }
        imageView.layoutIfNeeded()
    
        let recognizerLeft: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(recognizer:)))
        recognizerLeft.direction = .left
        let recognizerRight: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(recognizer:)))
        recognizerRight.direction = .right
        self.view.addGestureRecognizer(recognizerLeft)
        self.view.addGestureRecognizer(recognizerRight)
        
        self.loadingState(enable: false)
    }
    
    func loadingState(enable: Bool) {
        if enable {
            UIApplication.shared.beginIgnoringInteractionEvents()
            self.showWaitOverlay()
        } else {
            self.removeAllOverlays()
            if UIApplication.shared.isIgnoringInteractionEvents {
                UIApplication.shared.endIgnoringInteractionEvents()
            }
        }
    }
    
    @objc func handleSwipe(recognizer: UIGestureRecognizer) {
        if let swipeGesture = recognizer as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                presenter.swipe(left: false)
            case UISwipeGestureRecognizer.Direction.left:
                presenter.swipe(left: true)
            default:
                break
            }
        }
    }
    
    // MARK: - Alerts
    
    func showAlertView(with text: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "", message: text, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

