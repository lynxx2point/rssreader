//
//  DetailAssembly.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation

class DetailAssembly: DetailAssemblyProtocol {
    func assemble(with viewController: DetailViewController) {
        let presenter = DetailPresenter(view: viewController)
        let interactor = DetailInteractor(presenter: presenter)
        let router = DetailRouter(viewController: viewController)
        
        viewController.presenter = presenter
        presenter.interactor = interactor
        presenter.router = router
    }
}
