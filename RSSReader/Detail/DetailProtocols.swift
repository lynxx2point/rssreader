//
//  DetailProtocols.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation
import UIKit

protocol DetailViewProtocol: class {
    func initView(withImage: UIImage?, fromLeft: Bool?, forItem: ChannelItem)
    func showAlertView(with text: String)
    func loadingState(enable: Bool)
}

protocol DetailRouterProtocol: class {
    
}

protocol DetailAssemblyProtocol: class {
    func assemble(with viewController: DetailViewController)
}

protocol DetailPresenterProtocol: class {
    var view: DetailViewProtocol! { get set }
    var interactor: DetailInteractorProtocol! { get set }
    var router: DetailRouterProtocol! { get set }
    
    func configureView(channelData: [ChannelItem], itemIndex: Int)
    func showError(with text: String)
    func swipe(left: Bool)
    func showChannelItem(withImage: UIImage?)
}

protocol DetailInteractorProtocol: class {
    func loadImage(forItem: ChannelItem)
}
