//
//  DetailInteractor.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation
import UIKit

class DetailInteractor: DetailInteractorProtocol {
    weak var presenter: DetailPresenterProtocol!
    
    var largeImage: UIImage?
    
    init(presenter: DetailPresenterProtocol) {
        self.presenter = presenter
    }
    
    func loadImage(forItem: ChannelItem) {
        if let url = URL(string: forItem.imageFullSizeLink) {
            DispatchQueue.global(qos: .default).async {
                if let data = try? Data(contentsOf: url) {
                    self.largeImage = UIImage(data: data)
                } else {
                    self.largeImage = forItem.imagePreview
                }
                self.presenter.showChannelItem(withImage: self.largeImage)
            }
        } else {
            self.presenter.showChannelItem(withImage: nil)
        }
    }
}
