//
//  DetailPresenter.swift
//  RSSReader
//
//  Created by Alexandra Seliverstova on 25/10/2019.
//  Copyright © 2019 Alexandra Seliverstova. All rights reserved.
//

import Foundation
import UIKit

class DetailPresenter: DetailPresenterProtocol {
    weak var view: DetailViewProtocol!
    var interactor: DetailInteractorProtocol!
    var router: DetailRouterProtocol!
    
    var fromLeft: Bool?
    var channelData: [ChannelItem]!
    var itemIndex: Int!
    
    init(view: DetailViewProtocol) {
        self.view = view
    }
    
    func configureView(channelData: [ChannelItem], itemIndex: Int) {
        self.channelData = channelData
        self.itemIndex = itemIndex
        loadImage()
    }
    
    func showChannelItem(withImage: UIImage?) {
        DispatchQueue.main.async {
            self.view.initView(withImage: withImage, fromLeft: self.fromLeft, forItem:self.channelData[self.itemIndex])
            self.fromLeft = nil
        }
    }
    
    private func loadImage() {
        if self.channelData[self.itemIndex].imageFullSizeLink != "" {
            view.loadingState(enable: true)
            interactor.loadImage(forItem: self.channelData[self.itemIndex])
        } else {
            showChannelItem(withImage: nil)
        }
    }
    
    func swipe(left: Bool) {
        if left {
            showOtherNews(previous: false)
        } else {
            showOtherNews(previous: true)
        }
    }
    
    private func showOtherNews(previous: Bool) {
        if previous {
            if itemIndex != 0 {
                itemIndex-=1
                fromLeft = true
            } else {
                return
            }
        } else {
            if itemIndex != self.channelData.count - 1 {
                itemIndex+=1
                fromLeft = false
            } else {
                return
            }
        }
        loadImage()
    }
    
    func showError(with text: String) {
        view.showAlertView(with: text)
    }
}
